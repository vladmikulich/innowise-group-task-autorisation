import store from '../store'

export const authToLoginPage = (to, from, next) => {
  store.dispatch('checkLoginUser')
    .then(() => {
      next('/')
    })
    .catch(() => {
      next()
    })
}
export const authToHomePage = (to, from, next) => {
  store.dispatch('checkLoginUser')
    .then(() => {
      next()
    })
    .catch(() => {
      next('/login')
    })
}
