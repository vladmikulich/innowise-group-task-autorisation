import Vue from 'vue'
import VueRouter from 'vue-router'
import { authToHomePage, authToLoginPage } from './auth-guard'
import RegistrationPage from '../components/authorization/RegistrationPage.vue'
import SignInPage from '../components/authorization/SignInPage.vue'
import HomePage from '../components/HomePage.vue'
import Error404 from '../components/errors/404.vue'
import Cards from '../components/products/Cards.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/registration',
      name: 'Registration',
      component: RegistrationPage,
      meta: { requiresAuth: false }
    },
    {
      path: '/login',
      name: 'SignIn',
      component: SignInPage,
      meta: { requiresAuth: false }
    },
    {
      path: '/',
      name: 'HomePage',
      component: HomePage,
      meta: { requiresAuth: true }
    },
    {
      path: '/cards',
      name: 'Cards',
      component: Cards,
      meta: { requiresAuth: true }
    },
    {
      path: '/404',
      name: 'Error404',
      component: Error404
    },
    {
      path: '*',
      redirect: '/404'
    }
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    authToHomePage(to, from, next)
  } else if (to.matched.some(record => record.meta.requiresAuth === false)) {
    authToLoginPage(to, from, next)
  } else {
    next()
  }
})

export default router
