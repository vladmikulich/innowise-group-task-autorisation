import localforage from 'localforage'

export default {
  state: {
    lang: null
  },

  getters: {
    language (state) {
      return state.lang
    }
  },

  mutations: {
    setLocale (state, payload) {
      state.lang = payload
    }
  },

  actions: {
    // checkLangExistence ({ commit, dispatch }, payload) {
    //   return new Promise((resolve, reject) => {
    //     localforage.getItem(payload)
    //       .then(result => {
    //         commit('setLocale', result)
    //         resolve(result)
    //       })
    //       .catch((err) => {
    //         dispatch('setLocaleItem', { payload, value: process.env.VUE_APP_I18N_LOCALE || 'en' })
    //         reject(err)
    //       })
    //   })
    // },

    checkItemsExistence ({ commit, dispatch }, { name, defValue }) {
      return new Promise((resolve, reject) => {
        localforage.getItem(name)
          .then(result => {
            commit('setLocale', result)
            resolve(result)
          })
          .catch((err) => {
            dispatch('setLocaleItem', { name, value: defValue })
            reject(err)
          })
      })
    },

    setLocaleItem ({ commit, dispatch }, { name, value }) {
      return new Promise((resolve, reject) => {
        localforage.setItem(name, value)
          .then(() => {
            return dispatch('getLocale', name)
          })
          .then(resoult => {
            commit('setLocale', resoult)
            resolve()
          })
          .catch(err => reject(err))
      })
    },

    getLocale (context, name) {
      return new Promise((resolve, reject) => {
        localforage.getItem(name)
          .then(result => {
            resolve(result)
          })
          .catch(err => reject(err))
      })
    }
  }
}
