import axios from 'axios'

export default {
  state: {
    cards: [],
    categories: [],
    maxPrice: null
  },

  getters: {
    cards (state) {
      return state.cards
    },

    categories (state) {
      return state.categories
    },

    maxPrice (state) {
      return state.maxPrice
    }
  },

  mutations: {
    setCards (state, payload) {
      state.cards = payload
    },

    setCategories (state, payload) {
      state.categories = payload
    },

    setMaxPrice (state, payload) {
      state.maxPrice = payload
    }
  },

  actions: {
    fetchCards ({ commit }) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        axios.get(`http://www.mocky.io/v2/5e54fc3031000033b7eb3518`)
          .then(res => {
              commit('setCards', res.data)
              let categories = new Set()
              let maxPrice = 0
              res.data.forEach(element => {
                if (element.price > maxPrice) {
                  maxPrice = element.price
                }
                element.categories.forEach(category => categories.add(category))
              })
              commit('setCategories', categories)
              commit('setMaxPrice', maxPrice + 50)
              resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .finally(() => commit('setLoading', false))
      })
    }
  }
}
