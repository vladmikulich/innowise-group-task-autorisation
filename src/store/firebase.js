import * as fb from 'firebase/app/dist/index.cjs'
import firebaseConfig from '../firebaseConfig'

export default {
  state: {

  },

  getters: {

  },

  mutations: {

  },

  actions: {
    initFirebase () {
      fb.initializeApp(firebaseConfig)
    }
  }
}
