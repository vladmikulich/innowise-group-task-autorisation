import * as fb from 'firebase'

export default {
  state: {
    user: null,
    isUserLogin: false
  },

  getters: {
    user (state) {
      return state.user
    },
    isUserLogedIn (state) {
      return state.user !== null
    }
  },

  mutations: {
    setUser (state, payload) {
      state.user = payload
    },

    setUserStatus (state, payload) {
      state.isUserLogin = payload
    }
  },

  actions: {
    checkLoginUser ({ commit, dispatch }) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        fb.auth().onAuthStateChanged(user => {
          if (user) {
            commit('setLoading', false)
            commit('setUserStatus', true)
            dispatch('autoLogin', user)
            resolve()
          } else {
            commit('setLoading', false)
            reject(new Error('user does not exist'))
          }
        })
      })
    },

    registerUser ({ commit }, { email, password }) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        fb.auth().createUserWithEmailAndPassword(email, password)
          .then(user => {
            commit('setUser', { id: user.uid, email })
            resolve()
          })
          .catch((err) => {
            reject(err)
          })
          .finally(() => commit('setLoading', false))
      })
    },

    loginUser ({ commit }, { email, password }) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        fb.auth().signInWithEmailAndPassword(email, password)
          .then((res) => {
            commit('setUser', { id: res.user.uid, email })
            resolve(true)
          })
          .catch((err) => {
            reject(err)
          })
          .finally(() => commit('setLoading', false))
      })
    },

    signOutUser ({ commit, getters }) {
      fb.auth().signOut().then(() => {
        commit('setUser', null)
      })
    },

    autoLogin ({ commit }, payload) {
      commit('setUser', { id: payload.uid, email: payload.email })
    }
  }
}
