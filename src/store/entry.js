import * as fb from 'firebase'

export default {
  state: {
    entries: []
  },

  getters: {
    entries (state) {
      return state.entries
    }
  },

  mutations: {
    setEntry (state, payload) {
      if (state.entries === null) {
        state.entries = []
      }
      state.entries.push(payload)
    },
    loadEntries (state, payload) {
      state.entries = payload
    },
    updateEntry (state, { id, hours }) {
      const entry = state.entries.find(entr => {
        return entr.key === id
      })
      entry.hours = hours
    }
  },

  actions: {
    createEntry ({ commit, getters }, payload) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        const newEntry = {
          id: null,
          ownerId: getters.user.id,
          ...payload
        }
        fb.database().ref('entries').push(newEntry)
          .then(entry => {
            commit('setEntry', { ...newEntry, key: entry.key })
            resolve()
          })
          .catch((err) => {
            reject(err)
          })
          .finally(() => commit('setLoading', false))
      })
    },

    updateEntry ({ commit }, { id, hours }) {
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        fb.database().ref('entries').child(id).update({
          hours
        })
          .then(() => {
            commit('updateEntry', { id, hours })
            resolve()
          })
          .catch((err) => {
            reject(err)
          })
          .finally(() => commit('setLoading', false))
      })
    },

    fetchEntries ({ commit, getters }) {
      let resoult = []
      return new Promise((resolve, reject) => {
        commit('setLoading', true)
        fb.database().ref('entries').once('value')
          .then(fbVal => {
            const entries = fbVal.val()
            if (entries !== null) {
              Object.keys(entries).forEach(key => {
                const entry = entries[key]
                if (entry.ownerId === getters.user.id) {
                  resoult.push({ key, ownerId: entry.ownerId, date: entry.date, hours: entry.hours })
                }
              })
            } else {
              resoult = null
            }
            commit('loadEntries', resoult)
            resolve()
          })
          .catch((err) => {
            reject(err)
          })
          .finally(() => commit('setLoading', false))
      })
    }
  }
}
