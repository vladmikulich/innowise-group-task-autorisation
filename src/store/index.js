import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import firebase from './firebase'
import shared from './shared'
import entry from './entry'
import localforage from './localforage'
import card from './card'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user, firebase, shared, entry, localforage, card
  }
})
