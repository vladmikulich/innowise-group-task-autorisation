import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import Vuelidate from 'vuelidate'
import VueLogger from 'vuejs-logger'
import 'element-ui/lib/theme-chalk/index.css'
import './registerServiceWorker'
import i18n from './i18n'
import FlagIcon from 'vue-flag-icon'

const isProduction = process.env.NODE_ENV === 'production'
Vue.config.productionTip = false

const options = {
  isEnabled: true,
  logLevel: isProduction ? 'error' : 'debug',
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: '|',
  showConsoleColors: true
}

Vue.use(ElementUI, { locale })
Vue.use(Vuelidate)
Vue.use(FlagIcon)
Vue.use(VueLogger, options)

store.dispatch('initFirebase')

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
